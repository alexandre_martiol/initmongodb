<?php
/*
	Alexandre Martinez Olmos
	1r DAM
	10-06-2014
*/
	$conexion = new MongoClient();
	$database = $conexion->selectDB('ProgrammingCompany');
	
	$coleccion1 = $database->selectCollection('Project');
	$coleccion2 = $database->selectCollection('Language');
	$coleccion3 = $database->selectCollection('Leader');

	if(!empty($_GET['id'])){
		$id = $_GET['id'];
		
	//Una vez hecho el post, se añade toda la informacion a la variable proyecto y se edita en la base de datos
	} else {
		$id = $_POST['id'];
		$proyecto = array();
		$proyecto['nombre'] = $_POST['nombre'];
		$proyecto['descripcion'] = $_POST['descripcion'];
		$proyecto['language'] = $_POST['language'];
		$proyecto['leader'] = $_POST['leader'];
		
		$coleccion1->update(array('_id'=>new MongoId($id)), $proyecto);

		header("refresh: 0; url = http://localhost:63342/initMongoDB/index.php");
	}
	
	$proyecto = $coleccion1->findOne(array('_id'=>new MongoId($id)));
	
	echo "<form action='".$_SERVER['PHP_SELF']."' method='post'>";
	//Se pasa el id del elemento por el post para saber cual editar
	echo "<input type='hidden' name='id' value='".$id."'/>";
	echo "Nombre del proyecto: <input type='text' value='".$proyecto["nombre"]."' name='nombre'/><br>";
	echo "Descripcion: <input type='text' value='".$proyecto["descripcion"]."' name='descripcion'/><br>";
	
	echo "Lider: <select name='leader'>";
			$cursor = $coleccion3->find();
			while($cursor->hasNext()){
				$lider = $cursor->getNext();
				//Se guarda el id del elemento para hacer la relacion entre colecciones correctamente
				echo "<option value='".$lider['_id']."'>".$lider['nombre']."</option>";
			}					
	echo "</select></br>";
	
	echo "Lenguaje: <select name='language'>";
			$cursor = $coleccion2->find();
			while($cursor->hasNext()){
				$language = $cursor->getNext();
				//Se guarda el id del elemento para hacer la relacion entre colecciones correctamente
				echo "<option value='".$language['_id']."'>".$language['nombre']."</option>";						
			}					
	echo "</select></br>";

	echo "<input type='submit'/>";
    echo "<button type='cancel' onclick='javascript:window.location='http://localhost:63342/initMongoDB/index.php';'>Cancel</button>";
	echo "</form>";
?>

	

