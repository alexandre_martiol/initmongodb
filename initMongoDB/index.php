<?php
/*
	Alexandre Martinez Olmos
	1r DAM
	10-06-2014
*/
	//Conexion a la base de datos. Si no está creada se crea.
	$conexion = new MongoClient();
	$database = $conexion->selectDB('ProgrammingCompany');
	
	$coleccion = $database->selectCollection('Language');
	//Comprobamos si ya están creadas las variables para no hacerlo cada vez que ejecutamos el index.php
	$num = $coleccion->find()->count();
	if ($num == 0) {
		$phpLang = array();
		$phpLang['nombre'] = "PHP";
		$coleccion->insert($phpLang);
	
		$javaLang = array();
		$javaLang['nombre'] = "Java";
		$coleccion->insert($javaLang);
		
		$pythonLang = array();
		$pythonLang['nombre'] = "Python";
		$coleccion->insert($pythonLang);
		
		$cLang = array();
		$cLang['nombre'] = "C";
		$coleccion->insert($cLang);
	
		$coleccion = $database->selectCollection('Leader');
		
		$leader1 = array();
		$leader1['nombre'] = "Javier Tazon";
		$coleccion->insert($leader1);
		
		$leader2 = array();
		$leader2['nombre'] = "Alex Martinez";
		$coleccion->insert($leader2);
		
		$leader3 = array();
		$leader3['nombre'] = "Dani Santiago";
		$coleccion->insert($leader3);
	}
	
	$coleccion = $database->selectCollection('Project');
	$coleccion2 = $database->selectCollection('Language');
	$coleccion3 = $database->selectCollection('Leader');
	
	echo "<img src='img/avatar.png'></br>";
	echo "Bienvenido a la base de datos de proyectos de MyAppStand.</br>";
	echo "<a href='insertar.php'>Crear nuevo proyecto</a>";
	echo "</br>";
	echo "</br>";
	
	echo "Proyectos a&ntilde;adidos a la base de datos:</br>";
	//Se accede a todos los elementos de la coleccion y se va mostrando uno por uno la informacion del mismo
	$cursor = $coleccion->find();
	while($cursor->hasNext()){
		$proyecto = $cursor->getNext();
		echo "Nombre: <strong>".$proyecto['nombre']."</strong> | Despcripcion: ".$proyecto['descripcion']." | Lider: ";
		//para las siguientes variables se debe buscar el elemento que hace referencia a esa id y sacar de ahi el nombre.
		//Esto se debe a la relacion que se hace entre colecciones
		$lider = $coleccion3->findOne(array('_id'=>new MongoId($proyecto['leader'])));
		echo "".$lider['nombre']." | Lenguaje: ";
		$lenguaje = $coleccion2->findOne(array('_id'=>new MongoId($proyecto['language'])));
		echo "".$lenguaje['nombre']." <a href='editar.php?id=".$proyecto['_id']."'>Editar</a> <a href='borrar.php?id=".$proyecto['_id']."'>Eliminar</a><br>";
	}
?>
	
	
	
