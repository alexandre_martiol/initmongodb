<?php
/*
	Alexandre Martinez Olmos
	1r DAM
	10-06-2014
*/
	
	//Conexion a la base de datos. Si no está creada se crea.
	$conexion = new MongoClient();
	$database = $conexion->selectDB('ProgrammingCompany');
	
	$coleccion1 = $database->selectCollection('Project');
	
	$id = $_GET['id'];
	$coleccion1->remove(array('_id'=> new MongoId($id)));
	
	header("refresh: 0; url = http://localhost:63342/initMongoDB/index.php");
?>