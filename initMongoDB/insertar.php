<?php
/*
	Alexandre Martinez Olmos
	1r DAM
	10-06-2014
*/

	//Conexion a la base de datos. Si no está creada se crea.
	$conexion = new MongoClient();
	$database = $conexion->selectDB('ProgrammingCompany');
	
	$coleccion1 = $database->selectCollection('Project');
	$coleccion2 = $database->selectCollection('Language');
	$coleccion3 = $database->selectCollection('Leader');
	
	
	echo "Informacion del nuevo proyecto:</br>";
	echo "</br>";
	echo "<form action='".$_SERVER['PHP_SELF']."' method='post'>";
	
	echo "Nombre del proyecto: <input type='text' name='nombre'/><br>";
	
	echo "Descripcion: <input type='text' name='descripcion'/><br>";
	
	echo "Lider: <select name='leader'>";
			$cursor = $coleccion3->find();
			while($cursor->hasNext()){
				$lider = $cursor->getNext();
				//Se guarda el id del elemento para hacer la relacion entre colecciones correctamente
				echo "<option value='".$lider['_id']."'>".$lider['nombre']."</option>";
			}					
	echo "</select></br>";
	
	echo "Lenguaje: <select name='language'>";
			$cursor = $coleccion2->find();
			while($cursor->hasNext()){
				$language = $cursor->getNext();
				//Se guarda el id del elemento para hacer la relacion entre colecciones correctamente
				echo "<option value='".$language['_id']."'>".$language['nombre']."</option>";						
			}					
	echo "</select></br>";

	echo "<input type='submit'/>";
	echo "</form>";
	
	//Una vez hecho el post, se añade toda la informacion a la variable proyecto y se inserta en la base de datos
	if (!empty($_POST)){
        if ($_POST['nombre'] != "" && $_POST['descripcion'] != "") {
            try {
                $proyecto = array();
                $proyecto['nombre'] = $_POST['nombre'];
                $proyecto['descripcion'] = $_POST['descripcion'];
                $proyecto['language'] = $_POST['language'];
                $proyecto['leader'] = $_POST['leader'];
                $coleccion1->insert($proyecto);

                header("refresh: 0; url = http://localhost:63342/initMongoDB/index.php");

            } catch (MongoException $e) {
                die("Se ha producido un error: " . $e->getMessage());
            }
        }

        else {
            echo '<script language="javascript">';
            echo 'alert("There are empty fields!")';
            echo '</script>';
            header("refresh: 0; url = http://localhost:63342/initMongoDB/index.php");
        }
	}
?>